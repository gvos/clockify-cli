import requests
import dateutil.parser
import datetime
import os.path
import pickle
from pytz import timezone
from clockify_cli import utils


class Task:
    task_id = None
    project_id = None
    task_name = None

    def __init__(self, project_id, task_id, task_name):
        self.task_id = task_id
        self.project_id = project_id
        self.task_name = task_name


class DecoratedTask(Task):
    project_name = None

    def __init__(self, task, project_name):
        super(DecoratedTask, self).__init__(task.project_id, task.task_id, task.task_name)
        self.project_name = project_name


class Entry:
    description = None
    project_id = None
    task_id = None
    start = None
    end = None
    
    def __init__(self, start, end, project_id, task_id, description=None):
        self.start = start
        self.end = end
        self.project_id = project_id
        self.task_id = task_id
        self.description = description

    def date(self):
        return self.start.date()

    def duration(self):
        return self.end - self.start

    def slot(self):
        if self.start.second != 0 or self.start.minute != 0:
            raise Exception('Start time of Entry is not rounded to the hour: {}'.format(self.start))
        if self.end.second != 0 or self.end.minute != 0:
            raise Exception('End time of Entry is not rounded to the hour: {}'.format(self.end))
        return TimeSlot(self.start.hour, self.end.hour)


class DecoratedEntry(Entry):
    project_name = None
    task_name = None

    def __init__(self, entry, project_name, task_name):
        super(DecoratedEntry, self).__init__(entry.start, entry.end, entry.project_id, entry.task_id, entry.description)
        self.project_name = project_name
        self.task_name = task_name


class DayEntries:
    """
    Time entries for a specific day
    """
    entries = []

    def __init__(self, entries):
        self.entries = entries

    def date(self):
        return self.entries[0].date()

    def total_time(self):
        acc = None
        for entry in self.entries:
            if acc is None:
                acc = entry.duration()
            else:
                acc = acc + entry.duration()
        return acc

    def find_slots(self, hours):
        slots = []
        acc_hours = 0
        for h in range(9, 17):
            slot = TimeSlot(h, h + 1)
            overlaps = False
            for entry in self.entries:
                if slot.overlaps(entry):
                    overlaps = True
                    break
            if not overlaps:
                if len(slots) == 0:
                    slots.append(slot)
                elif slots[-1].is_contiguous(slot):
                    slots[-1] = slots[-1].merge(slot)
                else:
                    slots.append(slot)
                acc_hours = acc_hours + 1
                if acc_hours == hours:
                    return slots
        return None


class TimeSlot:
    start = None
    end = None

    def __init__(self, start_hour, end_hour):
        self.start = datetime.time(start_hour, 0)
        self.end = datetime.time(end_hour, 0)

    def __repr__(self):
        return '[{} - {}]'.format(self.start, self.end)

    def overlaps(self, entry):
        if self.start < entry.end.time() and self.start >= entry.start.time():
            return True
        if self.end > entry.start.time() and self.end <= entry.end.time():
            return True
        return False

    def is_contiguous(self, later_slot):
        return self.end == later_slot.start

    def merge(self, later_slot):
        return TimeSlot(self.start.hour, later_slot.end.hour)


