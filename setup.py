import setuptools

REQUIREMENTS = [i.strip() for i in open('requirements.txt').readlines()]

setuptools.setup(
    name='clockify-cli',
    version='0.0.1',
    description='TODO',
    author='TODO',
    packages=setuptools.find_packages(),
    install_requires=REQUIREMENTS,
    entry_points={
        'console_scripts':
            ['clockify = clockify_cli.clockify_main:entry_point']
    },
)